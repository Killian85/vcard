var regexCourriel = /.+@.+\..+/;

$(function () {

  $("input,textarea").blur(function () {
    if ($(this).val()) {
      $(this).parent().addClass("filled");
    } else {
      $(this).parent().removeClass("filled");
    }
  });

  $(".btn-resume").on("click", function () {
    $(this).toggleClass("active");
    $(".resume").toggleClass("active");
    $(".portfolio, .btn-portfolio, .contact-form, .btn-contact").removeClass("active");
  });

  $(".btn-portfolio").on("click", function () {
    $(this).toggleClass("active");
    $(".portfolio").toggleClass("active");
    $(".resume, .btn-resume, .contact-form, .btn-contact").removeClass("active");
  })

  $(".btn-contact").on("click", function () {
    $(this).toggleClass("active");
    $(".contact-form").toggleClass("active");
    $(".resume, .btn-resume, .portfolio, .btn-portfolio").removeClass("active");
  });

  $("#email").on("blur", function () {
    var valueMail = $("#email").val();
    if (!regexCourriel.test(valueMail)) {
      $("#email").css("border-bottom", "1px solid #d31515")
    } else {
      $("#email").css("border-bottom", "1px solid #fff")
    }
  });
  $(".contact-form input[type=submit]").on("click", function (e) {
    var valueMail = $("#email").val();
    if (!regexCourriel.test(valueMail)) {
      e.preventDefault();
      $("#email").css("border-bottom", "1px solid #d31515")
    } else {
      $("#email").css("border-bottom", "1px solid #fff")
      $(".resume, .btn-resume, .portfolio, .btn-portfolio, .contact-form, .btn-contact").removeClass("active")
      $(".portfolio__menu div").removeClass("active");
      $(".portfolio__menu #btn-all").addClass("active");

      if ($(".portfolio__menu #btn-all").attr("class", "active")) {
        $(".grid-wall .wall").addClass("active");
      }

      $("input[type='text'], input[type='email'], textarea").val("");
    }
  });

  $(".close").on("click", function () {
    $(".resume, .btn-resume, .portfolio, .btn-portfolio, .contact-form, .btn-contact").removeClass("active")
    $(".portfolio__menu div").removeClass("active");
    $(".portfolio__menu #btn-all").addClass("active");

    if ($(".portfolio__menu #btn-all").attr("class", "active")) {
      $(".grid-wall .wall").addClass("active");
    }
  });

  var $gallery = $(".grid-wall").isotope({

  });
  $(".portfolio__menu div").on("click", function () {
    $(".portfolio__menu div").removeClass("active");
    $(this).addClass("active");

    var filterValue = $(this).attr("data-filter");
    $gallery.isotope({
      filter: filterValue
    })
  });

  $(".grid-wall .active").on("click", function () {
    $(".overlay").after("<div class='view-box'><a href='#' class='close-wall'><i class='fa fa-times'></i></a><div class='view'>" + $(this).html() + "</div></div>").fadeIn();

    $(".overlay, .close-wall").on("click", function () {
      $(".view-box").remove();
      $(".overlay").fadeOut();
    });
  });

})
